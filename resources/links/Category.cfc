<cfcomponent extends="taffy.core.resource" taffy_uri="/links/{category}" hint="Returns list of all links within a category">
	<cffunction name="get" access="public" output="false">
		<!--- required --->
		<cfargument name="apiKey" type="string" required="true">
		<cfargument name="category" required="true" restargsource="Path" type="numeric" hint="Provide category id" />

			<cfquery name="q">
				SELECT company_name company, url, contact_person contact, phone, email, image, cat_name category
				FROM links l 
				LEFT JOIN LinksCats c ON l.cat = c.cat_id 
				WHERE status = <cfqueryparam cfsqltype="cf_sql_integer" value="1" />
				AND l.cat = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.category#" /> 
				ORDER BY l.sort
			</cfquery>

			<cfreturn representationOf(q).withStatus(200) />
	</cffunction>
</cfcomponent>