<cfcomponent extends="taffy.core.resource" taffy_uri="/videos/search/{count}" hint="Returns list of videos">
	<cffunction name="get" access="public" output="false">
		<!--- display settings --->
		<cfargument name="count" type="numeric" required="true" hint="Provide number of vidoes to show" />

		<cfargument name="keyword" type="string" required="false" hint="Provide a keyword to search for within a title" />
		<cfargument name="month_year" type="string" required="false" hint="Provide month and year combination from which to extract videos (4-2014)" />

		<cfquery name="q">
			SELECT TOP #count# id, title, embed, image, sort, date_create, description
			FROM videos
			WHERE 1 = <cfqueryparam cfsqltype="cf_sql_integer" value="1" />
			<cfif isDefined('keyword')>AND (title LIKE '%#keyword#%')</cfif>
			<cfif isDefined('month_year')>
				<cfif listLen(month_year,'-') eq 2>
					<cfset pMonth = listGetAt(month_year,1,'-')>
					<cfset pYear = listGetAt(month_year,2,'-')>
					<cfif pMonth eq 12><cfset nMonth = 1><cfset nYear = pYear + 1><cfelse><cfset nMonth = pMonth + 1><cfset nYear = pYear></cfif>
					<cfif pMonth lt 10><cfset pMonth = '0' & pMonth></cfif>
					<cfif nMonth lt 10><cfset nMonth = '0' & nMonth></cfif>
					AND date_create > '#pYear#-#pMonth#-01 00:00:00' 
					AND date_create < '#nYear#-#nMonth#-01 00:00:00' 
				</cfif>
			</cfif>
			ORDER BY date_create DESC
		</cfquery>

		<cfreturn representationOf(q).withStatus(200) />
	</cffunction>   
</cfcomponent>