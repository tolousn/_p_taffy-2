<cfcomponent extends="taffy.core.resource" taffy_uri="/buildings" hint="Returns list of buildings">
	<cffunction name="get" access="public" output="false">
  
		<!--- required --->
		<cfargument name="apiKey" type="string" required="true">
    
  		<!--- display settings --->
		<cfargument name="page" type="numeric" required="false" default="1" hint="Provide page number (1 or higher)" />
		<cfargument name="count" type="numeric" required="false" default="10" hint="Provide number of properties per page (10 / 20 / 40)" />
    
    	<!--- comma separated values --->
		<cfargument name="cat" type="string" required="false" default="" hint="Provide specific neighborhood id(s) [CSV]" />
		<cfargument name="amenities" type="string" required="false" default="" hint="Provide amenities limitation [CSV] (doorman / outdoorSpace / elevator / healthClub / pets / newConstruction / nofee)"  />
		<cfargument name="extras" type="string" required="false" default="" hint="Provide special limitation (featured / developments)"  />
    
    
		<!--- get all IDs --->
		<cfquery name="preq">
			SELECT id 
      		FROM buildings
			WHERE 1 = 1
      		<cfif cat neq ''>
				AND cat IN (#cat#)
			</cfif>
        	<cfif amenities neq ''>
				<cfloop list="#amenities#" index="a">
					AND #a# > 0
				</cfloop>
			</cfif>
			<cfif extras neq ''>
				<cfif extras eq 'featured'>AND featured = 1</cfif>
				<cfif extras eq 'developments'>AND new = 1</cfif>
			</cfif>
			ORDER BY date_update desc
		</cfquery>
    
		<!--- build ids --->
		<cfset ct = 0>
		<cfset appended = 0>
		<cfset done = "0">
		<cfset start = (page - 1) * count>
		<cfset ids = valueList(preq.id)>
    
		<cfloop list="#ids#" index="i">
		 	<cfif ct gte start>
				<cfset done = listAppend(done, i)>
				<cfset appended ++>
			</cfif>      
		  	<cfif appended eq count>
				<cfbreak>
			</cfif>      
			<cfset ct++>
		</cfloop>
    
    	<cfquery name="q">
			SELECT id, name, house, address, location, city, state, zip, public_note, contact, cat, status, type, propertyType, built, era,
			stories, units, families, bldgsize, lotsize, ext, minrent, maxrent, minbuy, maxbuy, beds, price, maintanance, taxes, exp, incrent,
			down, deduct, common, featured, fee, op, pets, new, nofee, elevator, doorman, brownstone, healthclub, pool, garage, subway,
			newconstruction, subwayline, openhouse, openhouseend, openhouse2, openhouseend2, style,  website, phone, laundry, bicycleroom,
			storage, nursery, lounge, valet, parking, concession, roofdeck, heating, cooling, wifi, keys, wheelchairaccess, commonoutdoorspace,
			receivingroom, businesscenter, virtualdoorman, gas, electricity, heat, water, imgcount, cableinternet, ac, greenbuilding,
			freightelevator, concierge, newdevelopment, highspeedinternet, maidservice, childrenplayroom, courtyard, driveway
      		FROM buildings b 
      		LEFT JOIN cats c ON b.cat = c.cat_id
      		WHERE b.id IN (#done#)
     		ORDER BY b.date_update desc
    	</cfquery>    
    
		<cfreturn representationOf(q).withStatus(200) />
	</cffunction>
</cfcomponent>