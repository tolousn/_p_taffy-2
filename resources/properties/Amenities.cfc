<cfcomponent extends="taffy.core.resource" taffy_uri="/properties/amenities/{id}" hint="Returns specific listing's amenities">
	<cffunction name="get" access="public" output="false">
		<!--- required --->
    	<cfargument name="apiKey" type="string" required="true">
		<cfargument name="id" type="numeric" required="true" hint="Provide id of the property" />
    
		<!--- get all IDs --->
		<cfquery name="q">
			SELECT balcony, bicycleroom, brownstone, businesscenter, common, commonoutdoorspace, concierge, dishwasher,
			doorman, duplex, eatinkitchen, elevator, families, fireplace, garage, granitekitchen, greenbuilding, hardwood,
			healthclub, highceilings, laundry, laundryinunit, loft, lounge, marblebath, multilevel, new, nursery, outdoorspace,
			parking, patio, pets, pool, renovated, roofdeck, storage, subway, terrace, triplex, walkincloset, washer
			FROM properties
			WHERE id = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.id#" />
		</cfquery>
			
		<cfreturn representationOf(q).withStatus(200) />
	</cffunction>
</cfcomponent>