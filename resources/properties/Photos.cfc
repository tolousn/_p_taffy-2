<cfcomponent extends="taffy.core.resource" taffy_uri="/properties/photos/{id}" hint="Returns specific listing's photos">
	<cffunction name="get" access="public" output="false">	
		<!--- required --->
		<cfargument name="apiKey" type="string" required="true">
		<cfargument name="id" type="numeric" required="true" hint="Provide id of the property" />

		<!--- get all IDs --->
		<cfquery name="q">
			SELECT name, filename, sort 
			FROM images
			WHERE listing_id = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.id#" />
		</cfquery>
			
		<cfreturn representationOf(q).withStatus(200) />
	</cffunction>
</cfcomponent>