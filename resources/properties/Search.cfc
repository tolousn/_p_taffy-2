<cfcomponent extends="taffy.core.resource" taffy_uri="/properties" hint="Returns list of listings">
	<cffunction name="get" access="public" output="false">
  
		<!--- required --->
		<cfargument name="apiKey" type="string" required="true">
    
		<!--- display settings --->
		<cfargument name="sort" type="string" required="false" default="price" hint="Provide sorting factor (price / size / date)" />
		<cfargument name="order" type="string" required="false" default="desc" hint="Provide sorting order (desc / asc)" />
		<cfargument name="page" type="numeric" required="false" default="1" hint="Provide page number (1 or higher)" />
		<cfargument name="perPage" type="numeric" required="false" default="10" hint="Provide number of properties per page (10 / 20 / 40)" />
    
    	<!--- comma separated values --->
		<cfargument name="status" type="string" required="false" default="2" hint="Provide specific status id(s) [CSV] (Sales - 1, Rentals - 2, In Contract - 11, Offer In - 12, App. Pending - 21, Sold - 19, Rented - 22)" />
		<cfargument name="cat" type="string" required="false" default="" hint="Provide specific neighborhood id(s) [CSV]" />
		<cfargument name="amenities" type="string" required="false" default="" hint="Provide amenities limitation [CSV] (doorman / outdoorSpace / elevator / healthClub / pets / newConstruction / nofee)"  />
    
    	<!--- numeric ranges --->
		<cfargument name="priceMin" type="numeric" required="false" default="0" hint="Provide minimum price" />
		<cfargument name="priceMax" type="numeric" required="false" default="100000000" hint="Provide maximum price" />
		<cfargument name="bedsMin" type="numeric" required="false" default="0" hint="Provide minimum bedroom count" />
		<cfargument name="bedsMax" type="numeric" required="false" default="10" hint="Provide maximum bedroom count" />
		<cfargument name="bathMin" type="numeric" required="false" default="0" hint="Provide minimum bathroom count" />
		<cfargument name="bathMax" type="numeric" required="false" default="10" hint="Provide maximum bathroom count" />
    
    	<!--- additional limitations --->
		<cfargument name="agent" type="numeric" required="false" default="0" hint="Provide specific agent id" />
		<cfargument name="extras" type="string" required="false" default="" hint="Provide special limitation (featured / furnished / developments / nofee / openhouse)"  />
    
		<!--- get all IDs --->
		<cfquery name="preq">
			SELECT id 
      		FROM properties
			WHERE status IN (#status#)
      		<cfif cat neq ''>AND cat IN (#cat#)</cfif>
        	<cfif amenities neq ''><cfloop list="#amenities#" index="a"> AND #a# > 0 </cfloop></cfif>
			AND price >= <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.priceMin#" />
			AND price <= <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.priceMax#" />
			AND beds >= <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.bedsMin#" />
			AND beds <= <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.bedsMax#" />
			AND bath >= <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.bathMin#" />
			AND bath <= <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.bathMax#" />
			<cfif agent neq 0>AND (contact = #agent# OR contact2 = #agent#)</cfif>
			<cfif extras neq ''>
				<cfif extras eq 'featured'>AND featured = 1</cfif>
				<cfif extras eq 'furnished'>AND furnished = 1</cfif>
				<cfif extras eq 'developments'>AND newdevelopments = 1</cfif>
				<cfif extras eq 'nofee'>AND nofee = 1</cfif>
				<cfif extras eq 'openhouse'>AND ( (openHouse > 0 AND openHouseEnd > DATEADD(hour, -1, getdate())) OR (openHouse2 > 0 AND openHouseEnd2 > DATEADD(hour, -1, getdate())) OR isohbyappt > 0 )</cfif>
			</cfif>
			ORDER BY 
			<cfif sort eq 'date'>date_update<cfelseif sort eq 'size'>beds<cfelse>price</cfif>
			<cfif order eq 'asc'>asc<cfelse>desc</cfif>
		</cfquery>
    
		<!--- build ids --->
		<cfset ct = 0>
		<cfset appended = 0>
		<cfset done = "0">
		<cfset start = (page - 1) * perPage>
		<cfset ids = valueList(preq.id)>

		<cfloop list="#ids#" index="i">
		  <cfif ct gte start><cfset done = listAppend(done, i)><cfset appended ++></cfif>      
		  <cfif appended eq perPage><cfbreak></cfif>      
			<cfset ct++>
		</cfloop>

		<cfquery name="q">
			SELECT p.id main_id,
			CASE WHEN p.hideaddress <> 1 THEN p.house + ' ' + p.address + ' ' + p.apt + ' ' + c.cat_name ELSE p.location + ' ' + c.cat_name END main_address, 
			CASE WHEN p.status = 1 THEN 'For Sale' 
			WHEN p.status = 2 THEN 'For Rent' 
			WHEN p.status = 11 THEN 'In Contract' 
			WHEN p.status = 12 THEN 'Offer In' 
			WHEN p.status = 21 THEN 'App. Pending'
			WHEN p.status = 19 THEN 'Sold' 
			WHEN p.status = 22 THEN 'Rented' END main_status, 

			p.imgphoto main_image, 

			c.cat_name main_neighborhood, 

			p.type essentials_type, 
			p.beds essentials_beds, 
			p.bath essentials_bath,  
			p.rooms essentials_rooms, 
			p.size essentials_size, 
			p.units essentials_units, 

			a1.id agents_agent_id, 
			a1.firstName + ' ' + a1.lastName agents_agent_name, 
			a1.email agents_agent_email,
			CASE WHEN a1.mobile <> '' THEN a1.mobile ELSE a1.phone END agents_agent_phone,
			a1.image agents_agent_image,
			a2.id agents_coagent_id, 
			a2.firstName + ' ' + a2.lastName agents_coagent_name, 
			a2.email agents_coagent_email,
			CASE WHEN a2.mobile <> '' THEN a2.mobile ELSE a2.phone END agents_coagent_phone,
			a2.image agents_coagent_image,

			p.price financials_price,  

			p.featured extras_featured, 
			p.nofee extras_nofee, 
			CASE WHEN ( ( p.openHouse > 0 AND p.openHouseEnd > DATEADD(hour, -1, getdate()) ) OR ( p.openHouse2 > 0 AND p.openHouseEnd2 > DATEADD(hour, -1, getdate()) ) OR ( p.isohbyappt > 0  ) ) THEN 1 ELSE 0 END extras_openhouse
		  FROM properties p 
			LEFT JOIN cats c ON p.cat = c.cat_id
			LEFT JOIN agents a1 ON p.contact = a1.id
			LEFT JOIN agents a2 ON p.contact2 = a2.id
		  WHERE p.id IN (#done#)
		  ORDER BY 
			<cfif sort eq 'date'>p.date_update<cfelseif sort eq 'size'>p.beds<cfelse>p.price</cfif>
			<cfif order eq 'asc'>asc<cfelse>desc</cfif>

		</cfquery>    
				
		<cfreturn representationOf(q).withStatus(200) />
	</cffunction>
</cfcomponent>