<cfcomponent extends="taffy.core.resource" taffy_uri="/agents/testimonials/{id}" hint="Returns specific agent's testimonials">
	<cffunction name="get" access="public" output="false">
		<!--- required --->
		<cfargument name="apiKey" type="string" required="true">
		<cfargument name="id" required="true" restargsource="Path" type="numeric" />

		<cfquery name="q">
			SELECT id, commentor, content, date_create, image, title
			FROM testimonials
			WHERE agentID = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.id#" />    
			ORDER BY date_create DESC    
		</cfquery>    

		<cfreturn representationOf(q).withStatus(200) />
	</cffunction>
</cfcomponent>