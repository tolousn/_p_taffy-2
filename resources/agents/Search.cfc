<cfcomponent extends="taffy.core.resource" taffy_uri="/agents/search" hint="Returns list of agents">
	<cffunction name="get" access="public" output="false">
		<!--- required --->
		<cfargument name="apiKey" type="string" required="true">

		<!--- display settings --->
		<cfargument name="sort" type="string" required="false" default="last" hint="Provide sorting factor (first / last / office / date)" />
		<cfargument name="order" type="string" required="false" default="asc" hint="Provide sorting order (desc / asc)" />

		<!--- comma separated values --->
		<cfargument name="name" type="string" required="false" default="" hint="Provide agent's first or last name"  />
		<cfargument name="language" type="string" required="false" default="" hint="Provide a language an agent speaks" />
		<cfargument name="office" type="numeric" required="false" default="0" hint="Provide agent's office id" />

		<cfquery name="q">
			SELECT id, firstname, lastname, title, email, phone, mobile, fax, bio, quote, lang language, image, office
			FROM agents
			WHERE status IN (-1,1) 
			<cfif arguments.name neq ''>AND firstName + ' ' + lastName LIKE '%#arguments.name#%'</cfif>
			<cfif arguments.language neq ''>AND lang LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#arguments.language#%" /></cfif>
			<cfif arguments.office gt 0>AND office = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.office#" /></cfif>
			ORDER BY 
			<cfif sort eq 'date'>date_create<cfelseif sort eq 'office'>office<cfelseif sort eq 'first'>firstName<cfelse>lastName</cfif>
			<cfif order eq 'desc'>desc<cfelse>asc</cfif>
		</cfquery>

		<cfreturn representationOf(q).withStatus(200) />
	</cffunction>
</cfcomponent>